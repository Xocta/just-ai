package bot.models;

import bot.BotApp;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

@Data
@NoArgsConstructor
@Component
public class VkBot {
    @Value("${app.access_token}")
    private String accessToken;
    @Value("${app.base_url}")
    private String baseUrl;
    @Value("${app.secret}")
    private String secret;
    @Value("${app.approve}")
    private String approve;
}
