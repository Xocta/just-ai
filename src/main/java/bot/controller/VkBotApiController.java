package bot.controller;

import bot.controller.json.InputJson;
import bot.models.VkBot;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class VkBotApiController {
    private final VkBot bot;

    @PostMapping
    public String serverController(@RequestBody InputJson message, HttpServletRequest request) {
        return switch (message.getType()) {
            case "confirmation" -> serverApprove(message);
            case "message_new" -> serverNewMessage(message);
            default -> "ok";
        };
    }

    private String serverNewMessage(InputJson message) {
        try {
            URL url = new URL(bot.getBaseUrl() + message.getObject().getMessage().getSender() + "&random_id=" + message.getObject().getMessage().getRandom() + "&message=" + message.getObject().getMessage().getText() + "&v=" + message.getVersion() + "&access_token=" + bot.getAccessToken());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.getResponseCode();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return "ok";
    }

    private String serverApprove(InputJson message) {
        return bot.getApprove();
    }
}
