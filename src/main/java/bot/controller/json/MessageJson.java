package bot.controller.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MessageJson {
    private String date;
    private String text;
    @JsonProperty("from_id")
    private String sender;
    @JsonProperty("random_id")
    private String random;
}
