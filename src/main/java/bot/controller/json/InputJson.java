package bot.controller.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class InputJson {
    private String type;
    @JsonProperty("group_id")
    private String groupId;
    @JsonProperty("event_id")
    private String eventId;
    private ObjectJson object;
    private String secret;
    @JsonProperty("v")
    private String version;
}
