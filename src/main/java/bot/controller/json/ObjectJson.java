package bot.controller.json;

import lombok.Data;

@Data
public class ObjectJson {
    private MessageJson message;
}
